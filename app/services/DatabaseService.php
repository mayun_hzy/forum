<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/6
 * Time: 15:50
 */
$config = require __DIR__ . '/../config/db.php';
static $db;
if ($db === null) {
    $mysql = mysqli_connect($config['host'], $config['username'], $config['password'], $config['database']);
    if (mysqli_connect_errno()) {
        exit(mysqli_connect_error());
    }
    $db = &$mysql;
    mysqli_set_charset($db, $config['charset']);
}
return $db;