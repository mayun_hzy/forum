<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/7
 * Time: 11:06
 */


const IS_LOGIN = 'is_login';
const USER_DATA = 'user_data';

session_start();

/**
 * 检查用户是否登录
 * @return bool
 */
function checkLogin()
{
    if (isset($_SESSION[IS_LOGIN])) {
        return $_SESSION[IS_LOGIN];
    } else {
        return false;
    }
}

/**
 * @return null|array
 */
function getUser()
{
    if (checkLogin()) {
        return $_SESSION[USER_DATA];
    } else {
        return null;
    }
}

function setUserLogin($user)
{
    $_SESSION[IS_LOGIN] = true;
    $_SESSION[USER_DATA] = $user;
}

function logout()
{
    session_unset();
    session_destroy();
}