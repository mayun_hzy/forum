<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/6
 * Time: 11:07
 */

include_once 'router.php';
require_once __DIR__ .  '/../controller/IndexController.php';

class Request{
    private $method;
    private $herader = [];
    private $data = [];
    private $service = [];
    private $router= null;
    private $response = '';
    
    public function __construct()
    {
        $this->service = $_SERVER;
        $this->router = new Router();
    }

    public function file()
    {
        $this->serializeHeader();
        $this->serializeMethod();
        $this->serializeRequestData();
        $this->fileRouter();
    }

    protected function serializeHeader()
    {

    }

    protected function serializeMethod() {
        $this->method = $this->service['REQUEST_METHOD'];
    }

    protected function serializeRequestData() {
        $this->data = $_REQUEST;
        unset($_REQUEST);
    }

    protected function fileRouter() {
        $router = isset($this->service['PATH_INFO']) ? $this->service['PATH_INFO'] : '/';
        $get = $this->router->getGetRouter();
        if (array_key_exists($router, $get)) {
            $con = explode('@', $get[$router]);
            $controller = new $con[0]();
            $this->response = call_user_func(array($controller, $con[1]), $this);
            return;
        }

        $post = $this->router->getPostRouter();
        if (array_key_exists($router, $post)) {
            $con = explode('@', $get[$router]);
            $controller = new $con[0]();
            $this->response = call_user_func(array($controller, $con[1]), $this);
            return;
        }
        $this->response = '404 not find';
    }

    public function echo()
    {
        return $this->response;
    }
}