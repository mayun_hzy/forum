<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/7
 * Time: 16:18
 */

/**
 * @param int $code
 * @param array $data
 */
if (!function_exists('responseError')) {
    function responseError($code, $data)
    {
        $array = ['code' => $code, 'data' => $data];
        die(json_encode($array));
    }
}

if (!function_exists('responseSuccess')) {
    function responseSuccess($data)
    {
        $array = ['code' => 0, 'data' => $data];
        die(json_encode($array));
    }
}