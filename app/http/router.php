<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/6
 * Time: 11:22
 */

class Router{
    private $post;
    private $get;

    /**
     * @return mixed
     */
    public function getPostRouter()
    {
        return $this->post;
    }

    /**
     * @return mixed
     */
    public function getGetRouter()
    {
        return $this->get;
    }

    public function __construct()
    {
        $this->get = [
            '/index' => 'IndexController@index',
            '/' => 'IndexController@default'
        ];

        $this->post = [

        ];
    }
}