<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/8/22
 * Time: 14:58
 */

const RANDOM_TYPE_STRING_LOWER = 1;
const RANDOM_TYPE_NUMBER = 2;
const RANDOM_TYPE_STRING_UPPER = 3;
const RANDOM_TYPE_STRING_AND_NUMBER = 4;

if (!function_exists('randomStr')) {
    /**
     * @param $length
     * @param int $type
     * @return string
     */
    function randomStr($length, $type = RANDOM_TYPE_STRING_AND_NUMBER)
    {
        $strLower = range('a', 'z');
        $number = range(0, 9);
        $strUpper = range('A', 'Z');
        switch ($type) {
            case RANDOM_TYPE_STRING_LOWER:
                $str = implode('', $strLower);
                break;
            case RANDOM_TYPE_NUMBER:
                $str = implode('', $number);
                break;
            case RANDOM_TYPE_STRING_UPPER:
                $str = implode('', $strUpper);
                break;
            case RANDOM_TYPE_STRING_AND_NUMBER:
                $str = implode('', $strUpper) . implode('', $strLower) . implode('', $number);
                break;
            default:
                $str = implode('', $strLower);
                break;
        }
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, strlen($str) - 1);
            $code .= $str[$index];
        }
        return $code;
    }
}

if (!function_exists('randomColor')) {
    /**
     * 随机生成一个颜色
     *
     * @param resource $img 资源
     * @param int $min 最小的颜色值
     * @param int $max 最大的颜色值
     * @return int
     */
    function randomColor($img, $min = 0, $max = 255)
    {
        return imagecolorallocate($img, mt_rand($min, $max), mt_rand($min, $max), mt_rand($min, $max));
    }
}