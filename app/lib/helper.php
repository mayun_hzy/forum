<?php
/**
 * Created by PhpStorm.
 * User: YongXuan.Huang
 * Date: 2017/9/10
 * Time: 14:39
 */

if (!function_exists('printNotify')) {
    function printNotify()
    {
        if (isset($_SESSION['error_code'])) {
            $type = $_SESSION['error_code'] == 0 ? 'alert-success' : 'alert-danger';
            echo <<<def
                <div class="alert alert-dismissible {$type}">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>!!!</strong> {$_SESSION['error_data']}
                </div>
def;
            unset($_SESSION['error_code']);
        }
    }
}