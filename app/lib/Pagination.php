<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/8
 * Time: 8:58
 */


/**
 * @param $last_page
 * @param $current_page
 * @param $parameter
 * @param string $class
 */
function pagination($last_page, $current_page, $parameter, $class = 'pagination')
{
    if ($last_page <= 1){
        return;
    }
    echo '<ul class="' . $class . '">';
    $f = $current_page == 1 ? 'disabled' : '';
    $prev = $current_page - 1 > 1 ? $current_page - 1 : 1;
    echo "<li class='{$f}'><a href='?{$parameter}&page=1' {$f}>&laquo;</a></li>";
    echo "<li class='{$f}'><a href='?{$parameter}&page={$prev}' {$f}>&larr;</a></li>";
    $show = 8;
    $z = $show / 2;
    $start = $current_page <= $z ? 1 : $current_page - $z;
    $end = $start + $show;
    if ($end > $last_page) {
        $end = $last_page;
    }

    if ($show + $start > $last_page) {
        $start = $last_page - $show;
    }
    if ($start > 1) {
        echo '<li class="disabled"><a>...</a></li>';
    }
    if ($start < 1) {
        $start = 1;
    }
    for ($i = $start; $i <= $end; $i++) {
        $in = $i == $current_page ? 'active' : '';
        echo " <li class='{$in}'><a href='?{$parameter}&page={$i}'>$i</a></li>";
    }
    $d = $current_page == $last_page ? 'disabled' : '';
    $next = $current_page + 1 > $last_page ? $last_page : $current_page + 1;
    if ($end < $last_page) {
        echo '<li class="disabled"><a>...</a></li>';
    }
    echo "<li class='{$d}'><a href='?{$parameter}&page={$next}' {$d}>&rarr;</a></li>";
    echo "<li class='{$d}'><a href='?{$parameter}&page={$last_page}' {$d}>&raquo;</a></li>";
    echo "</ul>";
}