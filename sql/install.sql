-- 删除表
DROP DATABASE `forum`;
CREATE DATABASE `forum`;
USE `forum`;

-- 用户基本表
CREATE TABLE IF NOT EXISTS `users` (
	`id`         INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
	COMMENT '用户ID',
	`access`     VARCHAR(32)  NOT NULL
	COMMENT '账户',
	`password`   VARCHAR(64)  NOT NULL
	COMMENT '密码',
	`is_deleted` BOOLEAN               DEFAULT FALSE
	COMMENT '是否删除',
	`updated_at` TIMESTAMP             DEFAULT now(),
	`created_at` TIMESTAMP             DEFAULT now(),
	UNIQUE `unique_access`(`access`)
)
	ENGINE = innodb
	DEFAULT CHARSET = `utf8`;

INSERT INTO `users`
VALUES (1, 'admin', '$2y$10$uw5h/i6XLQNL0.e9NEYLPe3X2y1pv1NTjn9RidSO6iz4IozUD2y0S', 0, now(), now());

-- 用户详情表
CREATE TABLE IF NOT EXISTS `users_data` (
	`id`         INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
	COMMENT '自己的id',
	`users_id`   INT UNSIGNED NOT NULL
	COMMENT '用户ID',
	`nickname`   VARCHAR(32)           DEFAULT '新用户'
	COMMENT '用户昵称',
	`avatar`     VARCHAR(255)          DEFAULT ''
	COMMENT '头像',
	`sex`        ENUM ('0', '1')       DEFAULT NULL
	COMMENT '性别',
	`age`        TINYINT UNSIGNED      DEFAULT NULL
	COMMENT '年龄',
	`avocation`  VARCHAR(64)           DEFAULT NULL
	COMMENT '兴趣爱好',
	`address`    VARCHAR(64)           DEFAULT NULL
	COMMENT '家庭地址',
	`status`     TINYINT UNSIGNED ZEROFILL COMMENT '用户状态',
	`flag`       TINYINT UNSIGNED ZEROFILL COMMENT '用户级别（0=普通用户/ 1 = vip会员 / 2 = admin）',
	`is_deleted` BOOLEAN               DEFAULT FALSE
	COMMENT '是否删除',
	`updated_at` TIMESTAMP             DEFAULT now(),
	`created_at` TIMESTAMP             DEFAULT now(),
	INDEX `index_users_id`(`users_id`),
	FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
		ON DELETE CASCADE
		ON UPDATE CASCADE
)
	ENGINE = innodb
	DEFAULT CHARSET = `utf8`;

INSERT INTO `users_data` VALUES (1, 1, 'Admin', '#', 1, NULL, NULL, NULL, 1, 2, 0, now(), now());
-- 帖子分类
CREATE TABLE IF NOT EXISTS `topics_category` (
	`id`          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
	COMMENT '自己的id',
	`title`       VARCHAR(32)  NOT NULL
	COMMENT '分类标题',
	`description` VARCHAR(64)           DEFAULT NULL
	COMMENT '描述',
	`is_enable`   BOOLEAN               DEFAULT TRUE
	COMMENT '是否可用',
	`is_deleted`  BOOLEAN               DEFAULT FALSE
	COMMENT '是否删除',
	`updated_at`  TIMESTAMP             DEFAULT now(),
	`created_at`  TIMESTAMP             DEFAULT now()
)
	ENGINE = innodb
	DEFAULT CHARSET = `utf8`;

INSERT INTO `topics_category` (`title`) VALUE ('生活');
INSERT INTO `topics_category` (`title`) VALUE ('娱乐');
INSERT INTO `topics_category` (`title`) VALUE ('游戏');
INSERT INTO `topics_category` (`title`) VALUE ('旅游');

-- 帖子详情
CREATE TABLE IF NOT EXISTS `topics` (
	`id`                 INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
	COMMENT '自己的id',
	`users_id`           INT UNSIGNED NOT NULL
	COMMENT 'users id',
	`title`              VARCHAR(64)  NOT NULL
	COMMENT '帖子标题',
	`topics_category_id` INT UNSIGNED NOT NULL
	COMMENT '分类id',
	`description`        TEXT         NOT NULL
	COMMENT '帖子内容',
	`is_recommend`       BOOLEAN               DEFAULT FALSE
	COMMENT '帖子是否推荐',
	`is_deleted`         BOOLEAN               DEFAULT FALSE
	COMMENT '是否删除',
	`updated_at`         TIMESTAMP             DEFAULT now(),
	`created_at`         TIMESTAMP             DEFAULT now(),
	INDEX `index_users_id`(`users_id`),
	INDEX `index_category_id`(`topics_category_id`),
	FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (`topics_category_id`) REFERENCES `topics_category` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
)
	ENGINE = innodb
	DEFAULT CHARSET = `utf8`;

-- 帖子回复
CREATE TABLE IF NOT EXISTS `topics_reply` (
	`id`         INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
	COMMENT '自己的id',
	`users_id`   INT UNSIGNED NOT NULL
	COMMENT 'users id',
	`topics_id`  INT UNSIGNED NOT NULL
	COMMENT '帖子 id',
	`parent_id`  INT UNSIGNED          DEFAULT 0
	COMMENT '针对评论',
	`content`    TEXT         NOT NULL
	COMMENT '回复内容',
	`is_deleted` BOOLEAN               DEFAULT FALSE
	COMMENT '是否删除',
	`updated_at` TIMESTAMP             DEFAULT now(),
	`created_at` TIMESTAMP             DEFAULT now(),
	INDEX `index_topics_id`(`topics_id`),
	INDEX `index_users_id`(`users_id`),
	INDEX `index_parent_id`(`parent_id`),
	FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	FOREIGN KEY (`topics_id`) REFERENCES `topics` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE

)
	ENGINE = innodb
	DEFAULT CHARSET = `utf8`;

-- 友情链接
CREATE TABLE IF NOT EXISTS `friendly_link` (
	`id`          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
	COMMENT '自己的id',
	`title`       VARCHAR(64)  NOT NULL
	COMMENT '链接名称',
	`description` VARCHAR(128)          DEFAULT NULL
	COMMENT '描述',
	`link`        VARCHAR(255) NOT NULL
	COMMENT '链接',
	`is_deleted`  BOOLEAN               DEFAULT FALSE
	COMMENT '是否删除',
	`updated_at`  TIMESTAMP             DEFAULT now(),
	`created_at`  TIMESTAMP             DEFAULT now()
)
	ENGINE = MYISAM
	DEFAULT CHARSET = `utf8`;

CREATE TABLE IF NOT EXISTS `trash` (
	`id`           INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
	COMMENT '自己的id',
	`deletx_id`    INT UNSIGNED NOT NULL
	COMMENT '需要删除的id',
	`delete_table` VARCHAR(32)  NOT NULL
	COMMENT '删除谁的数据',
	`is_delete`    BOOLEAN               DEFAULT FALSE
	COMMENT '这个id 是否被删除',
	`updated_at`   TIMESTAMP             DEFAULT now(),
	`created_at`   TIMESTAMP             DEFAULT now()
)
	ENGINE = MYISAM
	DEFAULT CHARSET = `utf8`;