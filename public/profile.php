<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/7
 * Time: 17:17
 */
require __DIR__ . '/../app/services/SessionService.php';
require __DIR__ . '/../app/services/TopicCategoryService.php';
require __DIR__ . '/../app/services/UserService.php';
require __DIR__ . '/../app/services/TopicService.php';
$categories = getTopicCategory();
if (!checkLogin()) {
    header('location:/login.php');
    exit;
}
$user = getUser();
if (checkIsAdmin()) {
    $id = (int)(isset($_GET['id']) ? $_GET['id'] : $user['users_id']);
    if ($id == 0) {
        $id = $user['users_id'];
    }
    $user = getUserInfo(null, $id);
}
$topics = findTopicByUsers($user['users_id']);
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Profile</title>
    <link rel="stylesheet" href="./css/cosmo/bootswatch.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">App Name</a>
        </div>
        <div class="collapse navbar-collapse" id="nav">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        帖子分类<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($categories as $category) {
                            echo "<li style='line-height: 30px'><a href='index.php?category_id={$category['id']}'> {$category['title']} </a></li>";
                        } ?>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="search.php" method="get">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search" required name="question">
                </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <?php
                if (checkLogin()) {
                    echo "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><img src='{$_SESSION[USER_DATA]['avatar']}' class='img-circle' width='25' height='25'>{$_SESSION[USER_DATA]['access']}</a>";
                    echo "<ul class='dropdown-menu' role='menu'>";
                    echo "<li><a href='profile.php'>个人资料</a></li>";
                    if (checkIsAdmin()) {
                        echo "<li><a href='admin/index.php'>后台</a></li>";
                    }
                    echo "<li ><a href = '#' id = 'logout' >登出</a ></li ></ul ></li > ";
                } else {
                    echo '<li><a href="login.php">登录</a></li><li><a href="register.php">注册</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="container mh">
    <?php
    if (isset($_SESSION['error_code'])) {
        $type = $_SESSION['error_code'] == 0 ? 'alert-success' : 'alert-danger';
        echo <<<def
<div class="alert alert-dismissible {$type}">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>!!!</strong> {$_SESSION['error_data']}
</div>
def;
        unset($_SESSION['error_code']);
    }
    ?>
    <div class="col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading">修改密码</div>
            <div class="panel-body">
                <form method="post" action="p/changePassword.php">
                    <input type="hidden" class="hidden" id="users_id" name="users_id"
                           value="<?php echo $user['users_id']; ?>">
                    <div class="form-group">
                        <label for="old_pwd">旧的密码:</label>
                        <input type="password" required class="form-control" name="old_password" id="old_pwd">
                    </div>
                    <div class="form-group">
                        <label for="pwd">新的密码:</label>
                        <input type="password" required class="form-control" name="password" id="pwd">
                    </div>
                    <div class="form-group">
                        <label for="pwd_com">确认新的密码:</label>
                        <input type="password" required class="form-control" name="password_confirmed" id="pwd_com">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="提交" class="btn btn-block btn-default">
                    </div>
                </form>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">修改基本信息</div>
            <div class="panel-body">
                <form action="p/profile.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" class="hidden" name="users_id" value="<?php echo $user['users_id']; ?>">
                    <div class="text-center">
                        <img src="<?php echo $user['avatar']; ?>" width="100" height="100" class="img-circle">
                    </div>
                    <div class="form-group">
                        <label for="nickname">昵称:</label>
                        <input type="text" required class="form-control" name="nickname" id="nickname"
                               value="<?php echo $user['nickname']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="avatar">头像:</label>
                        <input type="file" class="form-control" name="avatar">
                    </div>
                    <div class="form-group">
                        <label>性别:</label>
                        <label>
                            <input type="radio" name="sex" value="0" <?php echo $user['sex'] == 0 ? 'checked' : ''; ?>>女
                        </label>
                        <label>
                            <input type="radio" name="sex" value="1" <?php echo $user['sex'] == 1 ? 'checked' : ''; ?>>男
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="age">年龄:</label>
                        <input type="number" class="form-control" name="age" id="age"
                               value="<?php echo $user['age']; ?>">
                    </div>

                    <div class="form-group">
                        <label for="avocation">爱好:</label>
                        <input type="text" class="form-control" name="avocation" id="avocation" placeholder="多个逗号隔开"
                               value="<?php echo $user['avocation']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="address">地址:</label>
                        <input type="text" class="form-control" name="address" id="address"
                               value="<?php echo $user['address']; ?>">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="提交" class="btn btn-block btn-default">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="panel panel-primary">
            <div class="panel-heading">发布的帖子</div>
            <div class="panel-body">
                <ul class="list-group">
                    <?php
                    foreach ($topics as $topic) {
                        echo "<li class='list-group-item'>
<a href='./topic.php?id={$topic['id']}'>{$topic['title']}</a>
<span class='pull-right'>{$topic['updated_at']}</span>
</li>";
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container"  style="text-align: center">
    <hr>
    <?php
    require_once __DIR__ . '/../app/services/FriendlyLinkService.php';
    showFriendlyLink();
    ?>
</div>
<script>
    $(document).ready(function () {
        $('#logout').click(function (e) {
            $.post('/p/logout.php', {}, function (res) {
                var d = JSON.parse(res);
                if (d.code === 0) {
                    document.location.reload();
                }
            });
        });
    });
</script>
</body>
</html>
