<?php
include_once __DIR__ . '/../app/services/TopicCategoryService.php';
include_once __DIR__ . '/../app/services/RegisterService.php';
$categories = getTopicCategory();
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <link rel="stylesheet" href="./css/cosmo/bootswatch.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">App Name</a>
        </div>
        <div class="collapse navbar-collapse" id="nav">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        帖子分类<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($categories as $category) {
                            echo "<li style='line-height: 30px'><a href='index.php?category_id={$category['id']}'> {$category['title']} </a></li>";
                        } ?>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="search.php" method="get">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search" name="question" required>
                </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="login.php">登录</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mh">
    <div class="col-lg-4 col-lg-offset-4">
        <?php
        $error_code = isset($_SESSION['error_code']) ? $_SESSION['error_code'] : 0;
        $error_data = isset($_SESSION['error_data']) ? $_SESSION['error_data'] : 0;
        $data = <<<DATA
<div class="alert alert-dismissible alert-danger">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong>Error!</strong> <a href="#" class="alert-link"> $error_code </a> $error_data
</div>
DATA;
        if ($error_code != 0) {
            $_SESSION['error_code'] = 0;
            echo $data;
        }
        ?>
        <div class="panel panel-primary">
            <div class="panel-heading">注册</div>
            <div class="panel-body">
                <form method="post" action="register.php">
                    <div class="form-group">
                        <label>账号:</label>
                        <input class="form-control" name="access" required>
                        <span class="">*长度必须在6-32位之间</span>
                    </div>
                    <div class="form-group">
                        <label>密码:</label>
                        <input class="form-control" name="password" required type="password">
                        <span class="">*长度必须在6-32位之间</span>
                    </div>
                    <div class="form-group">
                        <label>确认密码:</label>
                        <input class="form-control" name="password_confirmed" required type="password">
                        <span class="">*</span>
                    </div>
                    <div class="form-group clearfix">
                        <label style="display: block">验证码:</label>
                        <input class="form-control" name="code" required style="width: 45%;display: inline-block"
                               placeholder="点击图片可以刷新验证码">
                        <div style="display: inline-block; width: 50%;float: right">
                            <a href="#" id="ref_code" style="border: 1px solid #ccc;display: inline-block">
                                <img src="code.php?" width="113px" height="40px" id="code">
                            </a>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-success">注册</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container"  style="text-align: center">
    <hr>
    <?php
    require_once __DIR__ . '/../app/services/FriendlyLinkService.php';
    showFriendlyLink();
    ?>
</div>
<script>
    $(document).ready(function (e) {
        $('#ref_code').click(function (e) {
            e.preventDefault();
            var time = new Date().getTime();
            $('#code').attr('src', '/code.php?' + time);
        });
    });
</script>
</body>
</html>