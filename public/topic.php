<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/8
 * Time: 10:47
 */

include_once __DIR__ . '/../app/services/TopicService.php';
include_once __DIR__ . '/../app/services/TopicCategoryService.php';
include_once __DIR__ . '/../app/services/SessionService.php';
include_once __DIR__ . '/../app/services/UserService.php';
include_once __DIR__ . '/../app/services/TopicReplyService.php';
$db = require __DIR__ . '/../app/services/DatabaseService.php';
$topicId = (int)(isset($_GET['id']) ? $_GET['id'] : 0);
$topic = findTopicById($topicId);
$categories = getTopicCategory();
$user = null;
if (!empty($topic)) {
    $user = getUserInfo($db, $topic['users_id']);
}
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Topic</title>
    <link rel="stylesheet" href="./css/cosmo/bootswatch.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">App Name</a>
        </div>
        <div class="collapse navbar-collapse" id="nav">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        帖子分类<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($categories as $category) {
                            echo "<li style='line-height: 30px'><a href='index.php?category_id={$category['id']}'> {$category['title']} </a></li>";
                        } ?>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="search.php" method="get">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search" required name="question">
                </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <?php
                if (checkLogin()) {
                    echo "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><img src='{$_SESSION[USER_DATA]['avatar']}' class='img-circle' width='25' height='25'>{$_SESSION[USER_DATA]['access']}</a>";
                    echo "<ul class='dropdown-menu' role='menu'>";
                    echo "<li><a href='profile.php'>个人资料</a></li>";
                    if (checkIsAdmin()) {
                        echo "<li><a href='admin/index.php'>后台</a></li>";
                    }
                    echo "<li ><a href = '#' id = 'logout' >登出</a ></li ></ul ></li > ";
                } else {
                    echo '<li><a href="login.php">登录</a></li><li><a href="register.php">注册</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="container mh">
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <?php
                if (empty($topic)) {
                    echo '<h5 class="text-center text-muted">帖子不存在</h5>';
                } else {

                    $reply = getReplyByTopicId($topicId);
                    $outReply = '';
                    if (!empty($reply)) {
                        foreach ($reply as $item) {
                            $remove = '';
                            if ((checkLogin() && $item['users_id'] == $_SESSION[USER_DATA]['users_id']) || checkIsAdmin()) {
                                $remove = "<a class='pull-right remove-topic' href='#' data-id='{$item['id']}'><span class='glyphicon glyphicon-remove'></span></a>";
                            }
                            $reg = '/[\\\r\\\n]/';
                            $content = preg_replace($reg, '', $item['content']);
                            $outReply .= "<li class='list-group-item clearfix'>
    <div class='pull-left' style='margin: 5px'>
        <img src='{$item['avatar']}' width='50px' height='50px' class='img-circle'>
    </div>
    <div class='pull-left'>
        <b class='text-muted'>{$item['access']}</b>
        <p><STRONG>{$content}</STRONG></p>
        <i>{$item['updated_at']}</i>
    </div>
    {$remove}
</li>";
                        }
                    } else {
                        $outReply = "<li class='list-group-item text-center text-muted'>还没有人回复哦~</li>";
                    }
                    $description = preg_replace('/[\\\r\\\n]/', '', $topic['description']);
                    $output = <<<DIV
<div>
<h4 class="glyphicon glyphicon-header">:{$topic['title']}</h4>
</div>
<div class="h6" style="line-height: 25px;">{$description}</div>
<hr>
<ul class="reply list-group">{$outReply}</ul>
<form method="post" action="p/reply.php" content="">
    <input type="hidden" class="hidden" value="{$topicId}" name="topic_id">
    <textarea class="form-control" name="content" required style="max-width: 100%"></textarea>
    <br>
    <input type="submit" value="回复" class="btn btn-success btn-block">
</form>
DIV;
                    echo $output;
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-success">
            <div class="panel-heading">作者详细信息</div>
            <div class="panel-body">
                <?php
                if ($user != null) {
                    $flag = $user['flag'];
                    switch ($flag) {
                        case 0:
                            $flag = '会员';
                            break;
                        case 1:
                            $flag = 'vip';
                            break;
                        case 2:
                            $flag = 'Admin';
                            break;
                        default:
                            $flag = '会员';
                            break;
                    }

                    $sex = $user['sex'] == null ? 3 : $user['sex'];
                    switch ($sex) {
                        case 0:
                            $sex = '女';
                            break;
                        case 1:
                            $sex = '男';
                            break;
                        default:
                            $sex = '未填写';
                            break;
                    }
                    $age = $user['age'] == null ? '未填写' : $user['age'];
                    $avocation = $user['avocation'] == null ? '未填写' : $user['avocation'];
                    $address = $user['address'] == null ? '未填写' : $user['address'];
                    $output = <<<OUT
<div class='text-center'><img src='{$user['avatar']}' class='img-circle' width='140' height='140'></div>
<br>
<ul class="list-group">
  <li class="list-group-item"><p class="list-group-item-text text-center">{$user['nickname']} {$flag}</p></a>
  <li class="list-group-item"><p class="list-group-item-text text-center">性别:{$sex}</p></a>
  <li class="list-group-item"><p class="list-group-item-text text-center">年龄:{$age}</p></a>
  <li class="list-group-item"><p class="list-group-item-text text-center">爱好:{$avocation}</p></a>
  <li class="list-group-item"><p class="list-group-item-text text-center">地址:{$address}</p></a>
</ul>
OUT;
                    echo $output;
                } else {
                    echo '<h5 class="text-center">用户不存在</h5>';
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="container"  style="text-align: center">
    <hr>
    <?php
    require_once __DIR__ . '/../app/services/FriendlyLinkService.php';
    showFriendlyLink();
    ?>
</div>
<script>
    $(document).ready(function () {
        $('#logout').click(function (e) {
            $.post('/p/logout.php', {}, function (res) {
                var d = JSON.parse(res);
                if (d.code === 0) {
                    document.location.reload();
                }
            });
        });
        $('.remove-topic').click(function (e) {
            e.preventDefault();
            var id = parseInt($(this).attr('data-id'));
            if (isNaN(id) && id <= 0) {
                return;
            }
            $.post('p/del_topic_reply.php', {id: id}, function (res) {
                window.location.reload();
            })
        })
    });
</script>
</body>
</html>
