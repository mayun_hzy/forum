<?php
require_once __DIR__ . '/../../app/services/UserService.php';
require_once __DIR__ . '/../../app/services/SessionService.php';
require_once __DIR__ . '/../../app/services/FriendlyLinkService.php';
require_once __DIR__ . '/../../app/lib/helper.php';
if (!checkIsAdmin()) {
    header('location:../login.php');
    die;
}
$user = getUser();
$links = getAllFriendlyLink();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Admin Center</title>
    <link href="css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
</head>
<body>
<div class="logo">
    <h4><a href="index.php">Admin <strong>Center</strong></a></h4>
</div>
<nav id="sidebar" class="sidebar nav-collapse collapse">
    <ul id="side-nav" class="side-nav">
        <li><a href="index.php"><i class="fa fa-home"></i> <span class="name">欢迎</span></a></li>
        <li class="panel">
            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav" href="#users-collapse">
                <i class="fa fa-users"></i>
                <span class="name">用户管理</span>
            </a>
            <ul id="users-collapse" class="panel-collapse collapse">
                <li><a href="user/list.php">用户列表</a></li>
                <li><a href="user/add.php">用户添加</a></li>
            </ul>
        </li>
        <li class="panel">
            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav"
               href="#category-collapse">
                <i class="fa fa-flag"></i>
                <span class="name">栏目管理</span>
            </a>
            <ul id="category-collapse" class="panel-collapse collapse">
                <li><a href="category/list.php">栏目列表</a></li>
                <li><a href="category/add.php">栏目添加</a></li>
            </ul>
        </li>
        <li class="panel">
            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav"
               href="#topics-collapse">
                <i class="fa fa-tree"></i>
                <span class="name">帖子管理</span>
            </a>
            <ul id="topics-collapse" class="panel-collapse collapse">
                <li><a href="topic/list.php">帖子列表</a></li>
            </ul>
        </li>
        <li class="active"><a href="friendly_link.php"><i class="fa fa-link"></i><span class="name">友情链接</span></a></li>
        <li><a href="trash.php"><i class="fa fa-trash"></i> <span class="name">回收站</span></a></li>
    </ul>
</nav>
<div class="wrap">
    <header class="page-header">
        <div class="navbar">
            <ul class="nav navbar-nav navbar-right pull-right">
                <li class="hidden-xs">
                    <a href="#" id="settings" title="Settings" data-toggle="popover" data-placement="bottom">
                        <i class="glyphicon glyphicon-cog"></i>
                    </a>
                </li>
                <li class="hidden-xs dropdown">
                    <a href="#" title="Account" id="account" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                    </a>
                    <ul id="account-menu" class="dropdown-menu account" role="menu">
                        <li role="presentation" class="account-picture">
                            <img src="<?php echo $user['avatar'] ?>" alt="" class="img-circle" height="50px">
                            <?php echo $user['access'] ?>
                        </li>
                        <li role="presentation">
                            <a href="../profile.php" class="link" target="_blank"><i class="fa fa-user"></i>个人资料</a>
                        </li>
                    </ul>
                </li>
                <li class="visible-xs">
                    <a href="#" class="btn-navbar" data-toggle="collapse" data-target=".sidebar" title="">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <li class="hidden-xs"><a href="#" id="logout"><i class="glyphicon glyphicon-off"></i></a></li>
            </ul>
        </div>
    </header>
    <div class="content container">
        <h2 class="page-title">
            回收站
            <small>垃圾数据:</small>
        </h2>
        <div class="col-md-12">
            <?php printNotify();?>
            <table class="table table-bordered table-lg mt-lg mb-0">
                <thead>
                <tr>
                    <th>id</th>
                    <th>标题</th>
                    <th>描述</th>
                    <th>链接</th>
                    <th width="100px" class="text-center">操作</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($links as $link) {
                    echo '<tr>';
                    echo "<td>{$link['id']}</td>";
                    echo "<td>{$link['title']}</td>";
                    echo "<td>{$link['description']}</td>";
                    echo "<td>{$link['link']}</td>";
                    echo "<td class='btn-group'>
<a class='btn btn-success' href='p/edit_link.php?id={$link['id']}'><i class='fa fa-edit'></i></a>
<a class='btn btn-danger' onclick='deleteLink({$link['id']})'><i class='fa fa-trash'></i></a>
</td>";
                    echo '</tr>';
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="5">
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#add"><i
                                    class="fa fa-upload"></i> 添加
                        </button>
                        <div id="add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true" style="display: none;">
                            <form action="p/add_friendly_link.php" method="post">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                ×
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">添加友情链接</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>标题</label>
                                                <input type="text" class="form-control" name="title" required>
                                            </div>
                                            <div class="form-group">
                                                <label>描述</label>
                                                <input type="text" class="form-control" name="description" required>
                                            </div>
                                            <div class="form-group">
                                                <label>链接</label>
                                                <input type="text" class="form-control" name="link" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                                            </button>
                                            <button type="submit" class="btn btn-primary">提交</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script src="lib/jquery/dist/jquery.min.js"></script>
<script src="lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="js/app.js"></script>
<script src="js/settings.js"></script>
<script type="text/template" id="settings-template">
    <div class="setting clearfix">
        <div>侧边栏为准</div>
        <div id="sidebar-toggle" class="pull-left btn-group" data-toggle="buttons-radio">
            <% onRight = sidebar == 'right'%>
            <button type="button" data-value="left" class="btn btn-sm btn-default <%= onRight? '' : 'active' %>">左边
            </button>
            <button type="button" data-value="right" class="btn btn-sm btn-default <%= onRight? 'active' : '' %>">
                右边
            </button>
        </div>
    </div>
    <div class="setting clearfix">
        <div>侧边栏</div>
        <div id="display-sidebar-toggle" class="pull-left btn-group" data-toggle="buttons-radio">
            <% display = displaySidebar%>
            <button type="button" data-value="true" class="btn btn-sm btn-default <%= display? 'active' : '' %>">显示
            </button>
            <button type="button" data-value="false" class="btn btn-sm btn-default <%= display? '' : 'active' %>">隐藏
            </button>
        </div>
    </div>
</script>
<script>
    $(document).ready(function () {
        $('#logout').click(function (e) {
            e.preventDefault();
            $.post('/p/logout.php', {}, function (res) {
                var d = JSON.parse(res);
                if (d.code === 0) {
                    document.location.reload();
                }
            });
        });
    });

    function deleteLink(id) {
        $.post('p/del_link.php', {id: id}, function (res) {
            window.location.reload();
        });
    }

</script>
</body>
</html>