<?php
require_once __DIR__ . '/../../../app/services/UserService.php';
require_once __DIR__ . '/../../../app/services/SessionService.php';
require_once __DIR__ . '/../../../app/lib/Pagination.php';
require_once __DIR__ . '/../../../app/lib/helper.php';
if (!checkIsAdmin()) {
    header('location:../login.php');
    die;
}
$id = (int)(isset($_GET['id']) ? $_GET['id'] : 0);
if ($id == 0) {
    die('Not 404');
}
$user = getUserInfo(null, $id);
if ($user == null) {
    die('Not 404');
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Admin Center</title>
    <link href="../css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="../img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
</head>
<body>
<div class="logo">
    <h4><a href="../index.php">Admin <strong>Center</strong></a></h4>
</div>
<nav id="sidebar" class="sidebar nav-collapse collapse">
    <ul id="side-nav" class="side-nav">
        <li>
            <a href="../index.php"><i class="fa fa-home"></i> <span class="name">欢迎</span></a>
        </li>
        <li class="panel active">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#side-nav" href="#users-collapse">
                <i class="fa fa-users"></i>
                <span class="name">用户管理</span>
            </a>
            <ul id="users-collapse" class="panel-collapse collapse in">
                <li><a href="list.php">用户列表</a></li>
                <li><a href="add.php">用户添加</a></li>
            </ul>
        </li>
        <li class="panel">
            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav"
               href="#category-collapse">
                <i class="fa fa-flag"></i>
                <span class="name">栏目管理</span>
            </a>
            <ul id="category-collapse" class="panel-collapse collapse">
                <li><a href="../category/list.php">栏目列表</a></li>
                <li><a href="../category/add.php">栏目添加</a></li>
            </ul>
        </li>
        <li class="panel">
            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav"
               href="#topics-collapse">
                <i class="fa fa-tree"></i>
                <span class="name">帖子管理</span>
            </a>
            <ul id="topics-collapse" class="panel-collapse collapse">
                <li><a href="../topic/list.php">帖子列表</a></li>
            </ul>
        </li>
        <li><a href="../friendly_link.php"><i class="fa fa-link"></i><span class="name">友情链接</span></a></li>
        <li><a href="../trash.php"><i class="fa fa-trash"></i> <span class="name">回收站</span></a></li>
    </ul>
</nav>
<div class="wrap">
    <header class="page-header">
        <div class="navbar">
            <ul class="nav navbar-nav navbar-right pull-right">
                <li class="hidden-xs">
                    <a href="#" id="settings" title="Settings" data-toggle="popover" data-placement="bottom">
                        <i class="glyphicon glyphicon-cog"></i>
                    </a>
                </li>
                <li class="hidden-xs dropdown">
                    <a href="#" title="Account" id="account" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                    </a>
                    <ul id="account-menu" class="dropdown-menu account" role="menu">
                        <li role="presentation" class="account-picture">
                            <img src="<?php echo $user['avatar'] ?>" alt="" class="img-circle" height="50px">
                            <?php echo $user['access'] ?>
                        </li>
                        <li role="presentation">
                            <a href="../../profile.php" class="link" target="_blank"><i class="fa fa-user"></i>个人资料</a>
                        </li>
                    </ul>
                </li>
                <li class="visible-xs">
                    <a href="#" class="btn-navbar" data-toggle="collapse" data-target=".sidebar" title="">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <li class="hidden-xs"><a href="#" id="logout"><i class="glyphicon glyphicon-off"></i></a></li>
            </ul>
        </div>
    </header>
    <div class="content container">
        <h2 class="page-title">
            用户管理
            <small>修改用户：<?php echo $user['access'] ?></small>
        </h2>
        <div class="col-md-4 col-md-offset-4">
            <?php printNotify(); ?>
            <div class="panel panel-default">
                <div class="panel-heading">修改基本信息</div>
                <div class="panel-body">
                    <form action="../../p/profile.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" class="hidden" name="users_id" value="<?php echo $user['users_id']; ?>">
                        <div class="text-center">
                            <img src="<?php echo $user['avatar']; ?>" width="100" height="100" class="img-circle">
                        </div>
                        <div class="form-group">
                            <label for="nickname">昵称:</label>
                            <input type="text" required class="form-control" name="nickname" id="nickname"
                                   value="<?php echo $user['nickname']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="pwd">新的密码:</label>
                            <input type="password" class="form-control" name="password" id="pwd">
                        </div>
                        <div class="form-group">
                            <label for="avatar">头像:</label>
                            <input type="file" class="form-control" name="avatar">
                        </div>
                        <div class="form-group">
                            <label>性别:</label>
                            <label>
                                <input type="radio" name="sex"
                                       value="0" <?php echo $user['sex'] == 0 ? 'checked' : ''; ?>>女
                            </label>
                            <label>
                                <input type="radio" name="sex"
                                       value="1" <?php echo $user['sex'] == 1 ? 'checked' : ''; ?>>男
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="age">年龄:</label>
                            <input type="number" class="form-control" name="age" id="age"
                                   value="<?php echo $user['age']; ?>">
                        </div>

                        <div class="form-group">
                            <label for="avocation">爱好:</label>
                            <input type="text" class="form-control" name="avocation" id="avocation" placeholder="多个逗号隔开"
                                   value="<?php echo $user['avocation']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="address">地址:</label>
                            <input type="text" class="form-control" name="address" id="address"
                                   value="<?php echo $user['address']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="status">状态:</label>
                            <input type="text" class="form-control" name="status" id="status"
                                   value="<?php echo $user['status']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="flag">权限:</label>
                            <select id="flag" name="flag" class="form-control">
                                <option value="0" <?php echo $user['flag'] == 0 ? 'selected' : '' ?>>会员</option>
                                <option value="1" <?php echo $user['flag'] == 1 ? 'selected' : '' ?>>Vip</option>
                                <option value="2" <?php echo $user['flag'] == 2 ? 'selected' : '' ?>>Admin</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="提交" class="btn btn-block btn-default">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../lib/jquery/dist/jquery.min.js"></script>
<script src="../lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="../js/app.js"></script>
<script src="../js/settings.js"></script>
<script type="text/template" id="settings-template">
    <div class="setting clearfix">
        <div>侧边栏为准</div>
        <div id="sidebar-toggle" class="pull-left btn-group" data-toggle="buttons-radio">
            <% onRight = sidebar == 'right'%>
            <button type="button" data-value="left" class="btn btn-sm btn-default <%= onRight? '' : 'active' %>">左边
            </button>
            <button type="button" data-value="right" class="btn btn-sm btn-default <%= onRight? 'active' : '' %>">
                右边
            </button>
        </div>
    </div>
    <div class="setting clearfix">
        <div>侧边栏</div>
        <div id="display-sidebar-toggle" class="pull-left btn-group" data-toggle="buttons-radio">
            <% display = displaySidebar%>
            <button type="button" data-value="true" class="btn btn-sm btn-default <%= display? 'active' : '' %>">显示
            </button>
            <button type="button" data-value="false" class="btn btn-sm btn-default <%= display? '' : 'active' %>">隐藏
            </button>
        </div>
    </div>
</script>
<script>
    $(document).ready(function () {
        $('#logout').click(function (e) {
            e.preventDefault();
            $.post('/p/logout.php', {}, function (res) {
                var d = JSON.parse(res);
                if (d.code === 0) {
                    document.location.reload();
                }
            });
        });
        $('#add_user').submit(function (e) {
            e.preventDefault();
            var access = $('#access').val();
            var password = $('#password').val();
            $.post('../p/add_user.php', {access: access, password: password}, function (res) {
                window.location.reload();
            });
        })
    });
</script>
</body>
</html>