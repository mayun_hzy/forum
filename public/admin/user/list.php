<?php
require_once __DIR__ . '/../../../app/services/UserService.php';
require_once __DIR__ . '/../../../app/services/SessionService.php';
require_once __DIR__ . '/../../../app/lib/Pagination.php';
if (!checkIsAdmin()) {
    header('location:../login.php');
    die;
}
$user = getUser();
$question = isset($_GET['question']) ? $_GET['question'] : '';
$page = isset($_GET['page']) ? $_GET['page'] : 1;
if ($page < 1) {
    $page = 1;
}
$users = getUserByPagination($page, $question);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Admin Center</title>
    <link href="../css/application.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="../img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
</head>
<body>
<div class="logo">
    <h4><a href="../index.php">Admin <strong>Center</strong></a></h4>
</div>
<nav id="sidebar" class="sidebar nav-collapse collapse">
    <ul id="side-nav" class="side-nav">
        <li>
            <a href="../index.php"><i class="fa fa-home"></i> <span class="name">欢迎</span></a>
        </li>
        <li class="panel active">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#side-nav" href="#users-collapse">
                <i class="fa fa-users"></i>
                <span class="name">用户管理</span>
            </a>
            <ul id="users-collapse" class="panel-collapse collapse in">
                <li class="active"><a href="list.php">用户列表</a></li>
                <li><a href="add.php">用户添加</a></li>
            </ul>
        </li>
        <li class="panel">
            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav"
               href="#category-collapse">
                <i class="fa fa-flag"></i>
                <span class="name">栏目管理</span>
            </a>
            <ul id="category-collapse" class="panel-collapse collapse">
                <li><a href="../category/list.php">栏目列表</a></li>
                <li><a href="../category/add.php">栏目添加</a></li>
            </ul>
        </li>
        <li class="panel">
            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#side-nav"
               href="#topics-collapse">
                <i class="fa fa-tree"></i>
                <span class="name">帖子管理</span>
            </a>
            <ul id="topics-collapse" class="panel-collapse collapse">
                <li><a href="../topic/list.php">帖子列表</a></li>
            </ul>
        </li>
        <li><a href="../friendly_link.php"><i class="fa fa-link"></i><span class="name">友情链接</span></a></li>
        <li><a href="../trash.php"><i class="fa fa-trash"></i> <span class="name">回收站</span></a></li>
    </ul>
</nav>
<div class="wrap">
    <header class="page-header">
        <div class="navbar">
            <ul class="nav navbar-nav navbar-right pull-right">
                <li class="hidden-xs">
                    <a href="#" id="settings" title="Settings" data-toggle="popover" data-placement="bottom">
                        <i class="glyphicon glyphicon-cog"></i>
                    </a>
                </li>
                <li class="hidden-xs dropdown">
                    <a href="#" title="Account" id="account" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                    </a>
                    <ul id="account-menu" class="dropdown-menu account" role="menu">
                        <li role="presentation" class="account-picture">
                            <img src="<?php echo $user['avatar'] ?>" alt="" class="img-circle" height="50px">
                            <?php echo $user['access'] ?>
                        </li>
                        <li role="presentation">
                            <a href="../../profile.php" class="link" target="_blank"><i class="fa fa-user"></i>个人资料</a>
                        </li>
                    </ul>
                </li>
                <li class="visible-xs">
                    <a href="#" class="btn-navbar" data-toggle="collapse" data-target=".sidebar" title="">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <li class="hidden-xs"><a href="#" id="logout"><i class="glyphicon glyphicon-off"></i></a></li>
            </ul>
        </div>
    </header>
    <div class="content container">
        <h2 class="page-title">
            用户管理
            <small>用户列表</small>
        </h2>
        <div class="col-md-12">
            <form method="get" action="list.php" class="clearfix">
                <label class="form-group col-lg-4">
                    <input type="search" name="question" class="form-control" value="<?php echo $question; ?>">
                </label>
                <div class="form-group col-lg-4">
                    <input type="submit" required class="btn btn-success">
                </div>
            </form>
            <table class="table table-bordered table-lg mt-lg mb-0">
                <thead>
                <tr>
                    <th width="30px">uid</th>
                    <th width="50px">头像</th>
                    <th width="50px">账户</th>
                    <th>昵称</th>
                    <th>性别</th>
                    <th>年龄</th>
                    <th>爱好</th>
                    <th>地址</th>
                    <th width="80px" class="text-center">操作</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($users->items as $item) {
                    $sex = $item['sex'] == null ? 3 : $item['sex'];
                    switch ($sex) {
                        case 0:
                            $sex = '女';
                            break;
                        case 1:
                            $sex = '男';
                            break;
                        default:
                            $sex = '未设置';
                            break;
                    }
                    $out = "<tr>
                        <td class='text-center'>{$item['users_id']}</td>
                        <td class='text-center'><img src='{$item['avatar']}' class='img-circle' width='15' height='15'></td>
                        <td>{$item['access']}</td>
                        <td>{$item['nickname']}</td>
                        <td>{$sex}</td>
                        <td>{$item['age']}</td>
                        <td>{$item['avocation']}</td>
                        <td>{$item['address']}</td>
                        <td>
                            <a href='edit.php?id={$item['users_id']}' class='btn btn-success btn-xs'><i class='fa fa-edit'></i></a>
                            <a class='btn btn-danger btn-xs' onclick='deleteUser({$item['users_id']})'><i class='fa fa-trash'></i></a>
                        </td>
                    </tr>";
                    echo $out;
                }
                ?>

                </tbody>
                <tfoot>
                <tr>
                    <td colspan="9">
                        <?php
                        $parameter = 'question=' . $question;
                        pagination($users->last_page, $users->current_page, $parameter);
                        ?>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<script src="../lib/jquery/dist/jquery.min.js"></script>
<script src="../lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
<script src="../js/app.js"></script>
<script src="../js/settings.js"></script>
<script type="text/template" id="settings-template">
    <div class="setting clearfix">
        <div>侧边栏为准</div>
        <div id="sidebar-toggle" class="pull-left btn-group" data-toggle="buttons-radio">
            <% onRight = sidebar == 'right'%>
            <button type="button" data-value="left" class="btn btn-sm btn-default <%= onRight? '' : 'active' %>">左边
            </button>
            <button type="button" data-value="right" class="btn btn-sm btn-default <%= onRight? 'active' : '' %>">
                右边
            </button>
        </div>
    </div>
    <div class="setting clearfix">
        <div>侧边栏</div>
        <div id="display-sidebar-toggle" class="pull-left btn-group" data-toggle="buttons-radio">
            <% display = displaySidebar%>
            <button type="button" data-value="true" class="btn btn-sm btn-default <%= display? 'active' : '' %>">显示
            </button>
            <button type="button" data-value="false" class="btn btn-sm btn-default <%= display? '' : 'active' %>">隐藏
            </button>
        </div>
    </div>
</script>
<script>
    $(document).ready(function () {
        $('#logout').click(function (e) {
            e.preventDefault();
            $.post('/p/logout.php', {}, function (res) {
                var d = JSON.parse(res);
                if (d.code === 0) {
                    document.location.reload();
                }
            });
        });
    });

    function deleteUser(id) {
        $.post('../p/del_user.php', {id: id}, function (res) {
            window.location.reload();
        });
    }
</script>
</body>
</html>