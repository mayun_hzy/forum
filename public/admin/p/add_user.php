<?php
/**
 * Created by PhpStorm.
 * User: YongXuan.Huang
 * Date: 2017/9/10
 * Time: 14:33
 */
include_once __DIR__ . '/../../../app/http/ResponseResult.php';
include_once __DIR__ . '/../../../app/services/UserService.php';
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    return;
}
$data = $_POST;
$access = isset($data['access']) ? $data['access'] : null;
$password = isset($data['password']) ? $data['password'] : null;
if ($access || $password) {
    $_SESSION['error_code'] = 2002;
    $_SESSION['error_data'] = '参数不正确';
}
if (!checkUser($access)) {
    saveUsers(null, $access, $password);
    $_SESSION['error_code'] = 0;
    $_SESSION['error_data'] = '创建成功';
} else {
    $_SESSION['error_code'] = 2002;
    $_SESSION['error_data'] = '用户已存在';
}