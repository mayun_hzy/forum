<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/12
 * Time: 9:28
 */

require_once __DIR__ . '/../../../app/services/SessionService.php';
require_once __DIR__ . '/../../../app/services/UserService.php';
require_once __DIR__ . '/../../../app/lib/Pagination.php';
require_once __DIR__ . '/../../../app/lib/helper.php';
require_once __DIR__ . '/../../../app/services/TopicReplyService.php';
require_once __DIR__ . '/../../../app/services/DatabaseService.php';
if (!checkIsAdmin()) {
    header('location:../index.php');
    die;
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $type = isset($_POST['type']) ? $_POST['type'] : null;
    $id = (int)(isset( $_POST['id']) ?  $_POST['id'] : 0);
    if ($type == null || $id <= 0) {
        die();
    }
    $result = mysqli_query($db, "DELETE FROM $type WHERE id = $id");
    return $result;
}