<?php
/**
 * Created by PhpStorm.
 * User: YongXuan.Huang
 * Date: 2017/9/10
 * Time: 19:21
 */

include_once __DIR__ . '/../../../app/services/UserService.php';
include_once __DIR__ . '/../../../app/http/ResponseResult.php';

$id = isset($_POST['id']) ? $_POST['id'] : 0;
if (!checkIsAdmin() || $_SERVER['REQUEST_METHOD'] !== 'POST' || $id == 0) {
    header('HTTP/1.1 404 not find');
    die('404 Not Find');
}
if (deleteUser($id)) {
    responseSuccess('删除成功');
} else {
    responseSuccess('删除失败');
}