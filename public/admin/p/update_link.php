<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/12
 * Time: 11:23
 */
require_once __DIR__ . '/../../../app/services/UserService.php';
require_once __DIR__ . '/../../../app/services/SessionService.php';
require_once __DIR__ . '/../../../app/services/FriendlyLinkService.php';
require_once __DIR__ . '/../../../app/lib/helper.php';
if (!checkIsAdmin()) {
    header('location:../login.php');
    die;
}
$user = getUser();
$id = (int)(isset($_POST['id']) ? $_POST['id'] : 0);
$title = isset($_POST['title']) ? $_POST['title'] : null;
$description = isset($_POST['description']) ? $_POST['description'] : null;
$links = isset($_POST['link']) ? $_POST['link'] : null;
if ($id <= 0 || $title == null || $links == null) {
    $_SESSION['error_id'] = 1000;
    $_SESSION['error_data'] = '缺少参数';
    header('location:../friendly_link.php');
    die();
}
$link = findLinkById($id);
if (empty($link)) {
    $_SESSION['error_id'] = 2000;
    $_SESSION['error_data'] = '数据不存在';
    header('location:../friendly_link.php');
    die();
}

if (updateLink($id, ['title' => $title, 'description' => $description, 'link' => $links])) {
    $_SESSION['error_id'] = 0;
    $_SESSION['error_data'] = '修改成功';
}
header('location:../friendly_link.php');