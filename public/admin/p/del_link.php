<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/12
 * Time: 11:09
 */
require_once __DIR__ . '/../../../app/services/UserService.php';
require_once __DIR__ . '/../../../app/services/SessionService.php';
require_once __DIR__ . '/../../../app/services/FriendlyLinkService.php';
if (!checkIsAdmin()) {
    header('location:../../login.php');
    die;
}
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header('location:../friendly_link.php');
}
$id = (int)(isset($_POST['id']) ? $_POST['id'] : 0);
if ($id <= 0) {
    $_SESSION['error_code'] = 1000;
    $_SESSION['error_data'] = '参数错误';
} else if (deleteLink($id)) {
    $_SESSION['error_code'] = 0;
    $_SESSION['error_data'] = '删除成功';
}
header('location:../friendly_link.php');