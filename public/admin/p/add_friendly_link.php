<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/12
 * Time: 10:48
 */
require_once __DIR__ . '/../../../app/services/UserService.php';
require_once __DIR__ . '/../../../app/services/SessionService.php';
require_once __DIR__ . '/../../../app/services/FriendlyLinkService.php';
if (!checkIsAdmin()) {
    header('location:../../login.php');
    die;
}
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header('location:../friendly_link.php');
}
$title = isset($_POST['title']) ? $_POST['title'] : null;
$description = isset($_POST['description']) ? $_POST['description'] : null;
$link = isset($_POST['link']) ? $_POST['link'] : null;
if ($title == null || $link == null) {
    $_SESSION['error_code'] = 1000;
    $_SESSION['error_data'] = '参数错误';
} else if (createFriendlyLink(['title' => $title, 'description' => $description, 'link' => $link])) {
    $_SESSION['error_code'] = 0;
    $_SESSION['error_data'] = '创建成功';
}
header('location:../friendly_link.php');
