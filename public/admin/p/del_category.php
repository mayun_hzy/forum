<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/11
 * Time: 11:15
 */
require_once __DIR__ . '/../../../app/services/UserService.php';
require_once __DIR__ . '/../../../app/services/SessionService.php';
require_once __DIR__ . '/../../../app/lib/Pagination.php';
require_once __DIR__ . '/../../../app/lib/helper.php';
require_once __DIR__ . '/../../../app/services/TopicCategoryService.php';
if (!checkIsAdmin()) {
    header('location:../login.php');
    die;
}
$id = (int)(isset($_POST['id']) ? $_POST['id'] : 0);
if ($id <= 0) {
    die();
}
updateCategory($id, ['is_deleted' => 1]);