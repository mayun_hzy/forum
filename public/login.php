<?php
include_once __DIR__ . '/../app/services/TopicCategoryService.php';
include_once __DIR__ . '/../app/services/LoginService.php';
$categories = getTopicCategory();
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>登录</title>
    <link rel="stylesheet" href="./css/cosmo/bootswatch.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">App Name</a>
        </div>
        <div class="collapse navbar-collapse" id="nav">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        帖子分类<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($categories as $category) {
                            echo "<li style='line-height: 30px'><a href='index.php?category_id={$category['id']}'> {$category['title']} </a></li>";
                        } ?>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="search.php" method="get">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search" name="question" required>
                </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="register.php">注册</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="container mh">
    <div class="col-lg-4 col-lg-offset-4">
        <div class="alert alert-dismissible alert-danger" id="alter" style="display: none">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Error!</strong>
            <span id="error_msg"></span>
        </div>
        <div class="panel panel-success">
            <div class="panel-heading text-center">登录</div>
            <div class="panel-body">
                <form id="login">
                    <div class="form-group">
                        <label>账户:</label>
                        <input type="text" required id="access" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>密码:</label>
                        <input type="password" required id="password" class="form-control">
                    </div>

                    <div class="form-group clearfix">
                        <label style="display: block">验证码:</label>
                        <input class="form-control" id="code" required style="width: 45%;display: inline-block"
                               placeholder="点击图片可以刷新验证码">
                        <div style="display: inline-block; width: 50%;float: right">
                            <a href="#" id="ref_code" style="border: 1px solid #ccc;display: inline-block">
                                <img src="./code.php" width="110px" height="35px" id="code_t">
                            </a>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success btn-block">登录</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container"  style="text-align: center">
    <hr>
    <?php
    require_once __DIR__ . '/../app/services/FriendlyLinkService.php';
    showFriendlyLink();
    ?>
</div>
<script>
    $(document).ready(function () {
        $('#login').submit(function (e) {
            e.preventDefault();
            var access = $('#access');
            var password = $('#password');
            var code = $('#code');
            $.post('/p/login.php', {
                access: access.val(),
                password: password.val(),
                code: code.val()
            }, function (res) {
                var data = JSON.parse(res);
                if (data.code === 1003) {
                    freshenCode();
                    showAlter(data.data);
                } else if (data.code === 0) {
                    document.location.href = '/index.php';
                } else {
                    showAlter(data.data);
                }
            });
        });
        $('#ref_code').click(function (e) {
            e.preventDefault();
            freshenCode();
        });
    });
    
    function freshenCode() {
        var time = new Date().getTime();
        $('#code_t').attr('src', '/code.php?' + time);
        $('#code').val('');
    }

    function showAlter($text) {
        $('#alter').show();
        $('#error_msg').text($text);
    }

    function hideAlter() {
        $('#alter').hide();
    }
</script>
</body>
</html>