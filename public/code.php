<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/7
 * Time: 14:20
 */

require_once __DIR__ . '/../app/lib/Random.php';

/**
 * 创建一个150*50真彩的画布
 */
$image = imagecreatetruecolor(150, 50);

/**
 * 随机获得验证码5位
 */
$code = randomStr(5);
session_start();
$_SESSION['code'] = $code;

/**
 * 填充背景颜色
 */
imagefilledrectangle($image, 0, 0, 150, 50, randomColor($image, 255, 255));

/**
 * 遍历字符串 位每一个字符设置颜色和角度
 */
forIndex(mb_strlen($code), function ($i) use ($image, $code) {
    $w = 20 + 25 * $i;
    imagettftext(
        $image,
        mt_rand(20, 30),
        mt_rand(-30, 30),
        $w,
        mt_rand(30, 40),
        randomColor($image, 0, 122),
        '../app/lib/msyhbd.ttf',
        $code[$i]);
});

/**
 * 随机画100点
 */
forIndex(200, function () use ($image) {
    imagesetpixel($image, mt_rand(0, 150), mt_rand(0, 50), randomColor($image));
});

/**
 * 随机画10条线
 */
forIndex(10, function () use ($image) {
    imageline($image, mt_rand(0, 150), mt_rand(0, 50), mt_rand(0, 150), mt_rand(0, 50), randomColor($image));
});

/**
 * 随机画10个实心的椭圆
 */
forIndex(10, function () use ($image) {
    imagefilledellipse($image, mt_rand(0, 150), mt_rand(0, 50), mt_rand(0, 10), mt_rand(0, 10), randomColor($image));
});

/**
 * 随机画10个弧线
 */
forIndex(10, function () use ($image) {
    imagefilledarc(
        $image,
        mt_rand(0, 150),
        mt_rand(0, 50),
        mt_rand(20, 30),
        mt_rand(20, 30),
        mt_rand(0, 120),
        mt_rand(0, 240),
        randomColor($image),
        2);
});
/**
 * 设置response header
 * content-type:image/png
 */
header('content-type:image/png');

/**
 * 输出图片
 */
imagepng($image);

/**
 * 销毁资源
 */
imagedestroy($image);


/**
 * @param int $max
 * @param callable $callback
 */
function forIndex(int $max, callable $callback)
{
    for ($i = 0; $i < $max; $i++) {
        $callback($i);
    }
}