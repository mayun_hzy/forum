<?php
require_once __DIR__ . '/../app/services/SessionService.php';
require_once __DIR__ . '/../app/services/UserService.php';
include_once __DIR__ . '/../app/lib/Pagination.php';
$db = require __DIR__ . '/../app/services/DatabaseService.php';
$result = mysqli_query($db, 'SELECT `id`, `title` FROM `topics_category` WHERE `is_enable` = TRUE ');
$categories = mysqli_fetch_all($result, MYSQLI_ASSOC);
$question = isset($_GET['question']) ? $_GET['question'] : '';
$page = isset($_GET['page']) ? $_GET['page'] : 1;

if ($page < 1) {
    $page = 1;
}
//一共有多少数据
$total = 0;
// 每页显示几个
$per_page = 30;
// 当前页
$current_page = $page;
// 最后一页
$last_page = 0;

$result = mysqli_query($db, "select count(*) count from topics t, users u where t.title LIKE '%{$question}%' and t.is_deleted = FALSE and t.users_id = u.id;");
$total = mysqli_fetch_assoc($result)['count'];
$start = $current_page * $per_page - $per_page;
$result = mysqli_query($db, "select t.id, t.title, t.is_recommend, u.access from topics t, users u where t.title LIKE '%{$question}%' and t.is_deleted = FALSE and t.users_id = u.id LIMIT {$start},{$per_page};");
$topics = mysqli_fetch_all($result, MYSQLI_ASSOC);
$last_page = ceil($total / $per_page);
?>

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Search</title>
    <link rel="stylesheet" href="./css/cosmo/bootswatch.css">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">App Name</a>
        </div>
        <div class="collapse navbar-collapse" id="nav">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        帖子分类<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php foreach ($categories as $category) {
                            echo "<li style='line-height: 30px'><a href='index.php?category_id={$category['id']}'> {$category['title']} </a></li>";
                        } ?>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="search.php" method="get">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search" name="question" required
                           value="<?php echo $question ?>">
                </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <?php
                if (checkLogin()) {
                    echo "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'><img src='{$_SESSION[USER_DATA]['avatar']}' class='img-circle' width='25' height='25'>{$_SESSION[USER_DATA]['access']}</a>";
                    echo "<ul class='dropdown-menu' role='menu'>";
                    echo "<li><a href='profile.php'>个人资料</a></li>";
                    if (checkIsAdmin()) {
                        echo "<li><a href='admin/index.php'>后台</a></li>";
                    }
                    echo "<li ><a href = '#' id = 'logout' >登出</a ></li ></ul ></li > ";
                } else {
                    echo '<li><a href="login.php">登录</a></li><li><a href="register.php">注册</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<div class="container mh">
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">帖子列表</div>
            <div class="panel-body">
                <div class="list-group">
                    <?php foreach ($topics as $topic) {
                        $recommend = $topic['is_recommend'] ?
                            "<span class='pull-right' style='margin: 0 10px; color: red'><i class='glyphicon glyphicon-thumbs-up'></i></span>"
                            : "<span class='pull-right' style='margin: 0 10px;'><i class='glyphicon glyphicon-bookmark'></i></span>";

                        echo "<a class='list-group-item' href='topic.php?id={$topic['id']}'><span style='padding: 0 10px'>{$topic['id']}</span>{$topic['title']} {$recommend}<span class='badge'>{$topic['access']}</span></a>";
                    } ?>
                </div>

                <?php
                $param = 'question=' . $question;
                pagination($last_page, $current_page, $param);
                ?>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-primary">
            <div class="panel-heading">用户信息</div>
            <div class="panel-body">
                <?php
                if (checkLogin()) {
                    $user = getUser();
                    $flag = $user['flag'];
                    switch ($flag) {
                        case 0:
                            $flag = '会员';
                            break;
                        case 1:
                            $flag = 'vip';
                            break;
                        case 2:
                            $flag = 'Admin';
                            break;
                        default:
                            $flag = '会员';
                            break;
                    }

                    $sex = $user['sex'] == null ? 3 : $user['sex'];
                    switch ($sex) {
                        case 0:
                            $sex = '女';
                            break;
                        case 1:
                            $sex = '男';
                            break;
                        default:
                            $sex = '未填写';
                            break;
                    }
                    $age = $user['age'] == null ? '未填写' : $user['age'];
                    $avocation = $user['avocation'] == null ? '未填写' : $user['avocation'];
                    $address = $user['address'] == null ? '未填写' : $user['address'];
                    $output = <<<OUT
<div class='text-center'><img src='{$user['avatar']}' class='img-circle' width='140' height='140'></div>
<br>
<ul class="list-group">
  <li class="list-group-item"><p class="list-group-item-text text-center">{$user['nickname']} {$flag}</p></a>
  <li class="list-group-item"><p class="list-group-item-text text-center">性别:{$sex}</p></a>
  <li class="list-group-item"><p class="list-group-item-text text-center">年龄:{$age}</p></a>
  <li class="list-group-item"><p class="list-group-item-text text-center">爱好:{$avocation}</p></a>
  <li class="list-group-item"><p class="list-group-item-text text-center">地址:{$address}</p></a>
</ul>
<a href='profile.php' class='btn btn-block btn-success'>编辑我</a>
OUT;
                    echo $output;
                } else {
                    echo '<h5 class="text-center">未登录</h5>';
                }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="container" style="text-align: center">
    <hr>
    <?php
    require_once __DIR__ . '/../app/services/FriendlyLinkService.php';
    showFriendlyLink();
    ?>
</div>
<script>
    $(document).ready(function () {
        $('#logout').click(function (e) {
            $.post('/p/logout.php', {}, function (res) {
                var d = JSON.parse(res);
                if (d.code === 0) {
                    document.location.reload();
                }
            });
        });
    });
</script>
</body>
</html>