<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/8
 * Time: 11:40
 */
include_once __DIR__ . '/../../app/services/TopicReplyService.php';
$replyId = createReplyByPost();
$id = $_POST['topic_id'];
header('location:/topic.php?id=' . $id . '#' . $replyId);