<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/8
 * Time: 16:56
 */
include_once __DIR__ . '/../../app/services/UserService.php';
changePassword();
$id = isset($_POST['users_id']) ? $_POST['users_id'] : '';
if (checkIsAdmin()) {
    header('location:/admin/user/edit.php?id=' . $id);
} else {
    header('location:/profile.php?id=' . $id);
}