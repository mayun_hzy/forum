<?php
/**
 * Created by PhpStorm.
 * User: Ysll <754060604@qq.com>
 * Date: 2017/9/11
 * Time: 14:22
 */
require_once __DIR__ . '/../../app/services/UserService.php';
require_once __DIR__ . '/../../app/services/SessionService.php';
require_once __DIR__ . '/../../app/lib/Pagination.php';
require_once __DIR__ . '/../../app/lib/helper.php';
require_once __DIR__ . '/../../app/services/TopicReplyService.php';
if (!checkLogin()) {
    header('location:../login.php');
    die;
}
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = (int)(isset($_POST['id']) ? $_POST['id'] : 0);
    if ($id <= 0) {
        header('location:/index.php');
        die;
    }
    $reply = getTopicReplyById($id);
    if ($reply == null) {
        die;
    }
    if (!checkIsAdmin() || $reply['users_id'] != $_SESSION[USER_DATA]['users_id']) {
        die;
    }
    deleteReply($reply['id']);
} else {
    header('location:../login.php');
}